dotfiles
========

This repo uses [dotbot][dotbot] for bootstrapping; to install, simply
`./install`

Dependencies
------------
Shell: zsh (install dependencies before installing dotfiles)
- [zplug][zplug]
- dircolors

Editor: vim
- [vim-plug][vim-plug] (`:PlugInstall`/`:PlugUpdate`)
- [vimpager][vimpager] (optional)

Terminal emulator: urxvt

Fonts:

- Hack (main terminal font)
- DejaVu family (terminal fallback)
- Noto Sans Symbols{,2} (terminal fallback)

[dotbot]:   https://github.com/anishathalye/dotbot
[vim-plug]: https://github.com/junegunn/vim-plug
[zplug]: https://github.com/zplug/zplug
[vimpager]: https://github.com/rkitover/vimpager
