#!/usr/bin/zsh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Syntax highlighter options:
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
ZSH_HIGHLIGHT_STYLES[path]='bold'
#for solarized
ZSH_HIGHLIGHT_STYLES[comment]='fg=yellow,bold'

# Shell options
unsetopt sharehistory
#setopt extendedglob

# Aliases
alias ls='ls --color=tty -h --group-directories-first'
alias tar='tar -a'
alias where='where -s'

alias df='df -h'
alias du='du -h'

alias gl='git pull --ff-only'

